const Koa = require('koa');
const cors = require('@koa/cors');
const movies = require('../routes/movies');
const bodyParser = require('koa-bodyparser'); 
const {dbConection} = require("../database/config");

class Server{
  constructor(){
    this.app = new Koa();
    this.port = process.env.PORT;

    //conectar a la bd
    this.conectarDB();



    this.app.use(bodyParser());

    
    
    //midlewares
    this.middlewares();
    
    //rutas
    this.app.use(movies.routes());
    this.app.use(movies.allowedMethods());
  }

  middlewares(){
    //CORS
    this.app.use(cors());
  }


  async conectarDB(){
    await dbConection();
  }





  listen(){
    this.app.listen(this.port, ()=>{
      console.log('Servidor corriendo', this.port)
    })
  };

} 

module.exports = Server;