const {Schema, model} = require('mongoose');

const MoviesSchema = Schema({
  title:{
    type:String,
    required: [true, 'El titulo es obligatorio']
  },
  year:{
    type:String,
    required: [true, 'El año es obligatorio']
  },
  released:{
    type:String,
    required:[true, 'Released es obligatorio']
  },
  genre:{
    type:String,
    required:[true, 'genre es obligatorio']
  },
  director:{
    type:String,
    required:[true, 'director es obligatorio']
  },
  actors:{
    type:String,
    required:[true, 'Actores es obligatorio']
  },
  plot:{
    type:String,
    required: [true, 'Plot es obligatorio']
  },
  retings:{
    type:Number,
    required:[true, 'retings']
  }})

  MoviesSchema.methods.toJSON = function(){
    const {__v, _id, ...movie } = this.toObject();
    movie.uid = _id;
    return movie;
  }


  module.exports = model('Movies', MoviesSchema);

  