
const Movies = require("../models/movies");


const movieExists = async(title = '' ) =>{
  const movie = await Movies.findOne({title});
  if(movie){
    return `La pelicula: ${title} ya está registrado`;
  } 
}

const updatePlot = async(plot, find, replace)=>{

  const findPlot = await Movies.find({plot:find});
  let updateMovies = ""
    bdMovies = "";


    if(findPlot.length > 0 ){
      const id = findPlot.map(p=>p._id);

      updateMovies = await Movies.updateMany({plot: find},{$set:{plot:replace}});
      bdMovies = await Movies.find({_id:{$in : id } });
    }

    return {
      bdMovies,
      updateMovies
    }
}

module.exports = {
  movieExists,
  updatePlot
}