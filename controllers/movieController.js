const { validarCampos } = require("../middlewares/validar-campos");
const { movieExists, updatePlot } = require("../helpers/db-validators");

const Movies = require("../models/movies");

const moviesGet = async (context, next) => {
	const { limite = 5, desde = 0, s } = context.request.query;

	const total = await Movies.find({title:s}).skip(Number(desde)).limit(Number(limite));
	return (context.body = {
		msg: "Exito",
		total,
	});
};

const moviesPost = async (context, next) => {
  const {
		title,
		plot,
		find,
		replace,
	} = context.request.body;
  
  //Validacion de campos
	const estado = validarCampos(context.request.body);

	if (estado.length > 0) {
		return (context.body = { msg: estado });
	}

  //validacion pelicula existente
	const findMovie = await movieExists(title);

	if (findMovie) {
		return (context.body = { msg: findMovie });
	}

  //Actualizacion plot
	const { bdMovies, updateMovies } = await updatePlot(plot, find, replace);
  
  //Insercion Pelicula
	const movie = new Movies(context.request.body);
	await movie.save();
	return (context.body = { movie, bdMovies, updateMovies });
};

module.exports = {
	moviesGet,
	moviesPost,
};
