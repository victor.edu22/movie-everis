
const validarCampos = (datos) => { 
  if( datos.title == null || datos.title.length == 0) {
    return 'El campo title esta vacio';
  }
  if( datos.year == null || datos.year.length == 0) {
    return 'El campo year esta vacio';
  }
  if( datos.genre == null || datos.genre.length == 0) {
    return 'El genre title esta vacio';
  }
  if( datos.director == null || datos.director.length == 0) {
    return 'El campo director esta vacio';
  }
  if( datos.actors == null || datos.actors.length == 0) {
    return 'El campo actors esta vacio';
  }
  if( datos.plot == null || datos.plot.length == 0) {
    return 'El campo plot esta vacio';
  }
  if( datos.retings == null || datos.retings.length == 0) {
    return 'El campo retings esta vacio';
  }
  return [];
}

const valida = (param)=>{
  if( param == null || param.length == 0) {
    return param;
  }
  return false;
}

module.exports = {
  validarCampos
}